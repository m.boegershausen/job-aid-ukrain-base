package com.jobaidukraine.core.services.ports.in.queries;

import com.jobaidukraine.core.domain.User;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserQuery {
  User findById(long id);

  Page<User> findAllByPageable(Pageable pageable);

  Optional<User> findByEmail(String email);
}
