package com.jobaidukraine.core.services.ports.out;

import com.jobaidukraine.core.domain.Job;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface JobPort {
  Optional<Job> findById(long id);

  Page<Job> findAllByPageable(Pageable pageable);

  Page<Job> findAllByPageable(Pageable pageable, String title);

  Job update(Job job);

  void delete(long id);
}
