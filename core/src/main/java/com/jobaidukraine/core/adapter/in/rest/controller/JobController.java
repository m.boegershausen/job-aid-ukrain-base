package com.jobaidukraine.core.adapter.in.rest.controller;

import com.jobaidukraine.core.adapter.in.rest.assemblers.JobResourceAssembler;
import com.jobaidukraine.core.adapter.in.rest.dto.JobDto;
import com.jobaidukraine.core.adapter.in.rest.mapper.JobMapper;
import com.jobaidukraine.core.services.ports.in.queries.JobQuery;
import com.jobaidukraine.core.services.ports.in.usecases.JobUseCase;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1.0/jobs")
@RequiredArgsConstructor
@OpenAPIDefinition(
    info =
        @Info(
            title = "JobAidUkraine",
            version = "1.0",
            description = "JobAidUkraine REST API",
            license =
                @License(name = "MIT", url = "https://gitlab.com/jobaid/base/-/blob/main/LICENSE")),
    tags = {@Tag(name = "Job")})
public class JobController {

  private final JobUseCase jobUseCase;
  private final JobQuery jobQuery;
  private final PagedResourcesAssembler<JobDto> pagedResourcesAssembler;
  private final JobResourceAssembler jobResourceAssembler;
  private final JobMapper jobMapper;

  @Operation(
      summary = "Create a new job for a given company",
      tags = {"Job"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Job creation was successful",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = JobDto.class))),
        @ApiResponse(responseCode = "400", description = "Invalid job supplied"),
        @ApiResponse(responseCode = "404", description = "Company to save job to not found"),
      })
  @PostMapping
  public EntityModel<JobDto> save(
      @RequestBody JobDto job, @RequestParam(name = "companyId") Long companyId) {
    job = jobMapper.domainToDto(jobUseCase.save(jobMapper.dtoToDomain(job), companyId));
    return jobResourceAssembler.toModel(job);
  }

  @Operation(
      summary = "Get all jobs",
      tags = {"Job"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Job retrieval was successful",
            content =
                @Content(
                    mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = JobDto.class)))),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid pageable or optional title supplied"),
        @ApiResponse(responseCode = "404", description = "No job found")
      })
  @GetMapping
  public PagedModel<EntityModel<JobDto>> list(
      Pageable pageable, @RequestParam(required = false, name = "title") String title) {
    Page<JobDto> jobs = null;
    if (title != null) {
      jobs = this.jobQuery.findAllByPageable(pageable, title).map(jobMapper::domainToDto);
    } else {
      jobs = this.jobQuery.findAllByPageable(pageable).map(jobMapper::domainToDto);
    }
    return pagedResourcesAssembler.toModel(jobs, jobResourceAssembler);
  }

  @Operation(
      summary = "Get job by id",
      tags = {"Job"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Job retrieval was successful",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = JobDto.class))),
        @ApiResponse(responseCode = "400", description = "Invalid id supplied"),
        @ApiResponse(responseCode = "404", description = "Job not found")
      })
  @GetMapping(value = "/{id}")
  public EntityModel<JobDto> show(@PathVariable long id) {
    JobDto job = jobMapper.domainToDto(this.jobQuery.findById(id));
    return jobResourceAssembler.toModel(job);
  }

  @Operation(
      summary = "Update a job",
      tags = {"Job"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Job update was successful",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = JobDto.class))),
        @ApiResponse(responseCode = "400", description = "Invalid job or id supplied"),
      })
  @PatchMapping(value = "/{id}")
  @PreAuthorize(
      "hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_EDITOR') or hasAuthority('ROLE_USER')")
  public EntityModel<JobDto> update(@RequestBody JobDto job, @PathVariable long id) {
    job.setId(id);
    job = jobMapper.domainToDto(this.jobUseCase.update(jobMapper.dtoToDomain(job)));

    return jobResourceAssembler.toModel(job);
  }

  @Operation(
      summary = "Delete a job",
      tags = {"Job"})
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "200", description = "Job deletion was successful"),
        @ApiResponse(responseCode = "400", description = "Invalid id supplied"),
      })
  @DeleteMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @PreAuthorize(
      "hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_EDITOR') or hasAuthority('ROLE_USER')")
  public void delete(@PathVariable long id) {
    this.jobUseCase.delete(id);
  }
}
