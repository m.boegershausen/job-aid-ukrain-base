package com.jobaidukraine.core.adapter.in.rest.controller;

import com.jobaidukraine.core.adapter.in.rest.dto.ExperienceDto;
import com.jobaidukraine.core.adapter.in.rest.dto.JobResultDto;
import com.jobaidukraine.core.adapter.in.rest.dto.JobTypeDto;
import com.jobaidukraine.core.adapter.in.rest.dto.LanguageLevelDto;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.time.LocalDateTime;
import java.util.List;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1.0/jobsearch/")
@OpenAPIDefinition(
    info =
        @Info(
            title = "JobAidUkraine",
            version = "1.0",
            description = "JobAidUkraine REST API",
            license =
                @License(name = "MIT", url = "https://gitlab.com/jobaid/base/-/blob/main/LICENSE")),
    tags = {@Tag(name = "JobSearch")})
public class JobSearchController {

  @GetMapping
  @Operation(
      summary = "Search for jobs with parameters",
      tags = {"JobSearch"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "At least one job found",
            content =
                @Content(
                    mediaType = "application/json",
                    array = @ArraySchema(schema = @Schema(implementation = JobResultDto.class)))),
        @ApiResponse(
            responseCode = "400",
            description = "Invalid pageable or optional title supplied"),
        @ApiResponse(responseCode = "404", description = "No job found")
      })
  public PagedModel<EntityModel<JobResultDto>> search(
      Pageable pageable,
      @RequestParam(required = false, name = "q") String buzzwords,
      @RequestParam(required = false, name = "type") JobTypeDto type,
      @RequestParam(required = false, name = "exp") ExperienceDto experience,
      @RequestParam(required = false, name = "exp") List<LanguageLevelDto> languageLevels,
      @RequestParam(required = false, name = "cs") LocalDateTime changedSince,
      @RequestParam(required = false, name = "lat") Float latitude,
      @RequestParam(required = false, name = "long") Float longitude,
      @RequestParam(required = false, name = "dist") Integer distance) {
    throw new NotImplementedException();
  }
}
