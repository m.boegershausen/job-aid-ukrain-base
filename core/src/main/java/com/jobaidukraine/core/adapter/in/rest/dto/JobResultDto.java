package com.jobaidukraine.core.adapter.in.rest.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDateTime;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JobResultDto {
  @Schema(
      description =
          "The database generated job ID. "
              + "This hast to be null when creating a new job. "
              + "This has to be set when updating an existing job.",
      example = "1")
  private Long id;

  @JsonDeserialize(using = LocalDateTimeDeserializer.class)
  @JsonSerialize(using = LocalDateTimeSerializer.class)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  @Schema(description = "The last update date of the job.", example = "2020-01-01T12:00:00.000Z")
  private LocalDateTime lastChange;

  @Schema(
      description = "The relevance score this result achieved with the given search criterias",
      example = "1")
  private Integer searchScore;

  @Schema(description = "The job title.", required = true, example = "Software Engineer")
  private String title;

  @Schema(description = "The job type.", example = "Full-time")
  private String jobType;

  @Valid
  @Schema(description = "The address of the job offer")
  private AddressDto address;
}
