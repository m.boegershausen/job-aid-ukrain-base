package com.jobaidukraine.core.adapter.in.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import javax.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "Address", description = "An address for companys, jobs, housing, etc.")
public class AddressDto {

  @Min(value = -90, message = "Latitude must be between -90 and 90")
  @Max(value = 90, message = "Latitude must be between -90 and 90")
  @Schema(description = "The latitude coordinate of the address", example = "51.5109688")
  private Float latitude;

  @Min(value = -180, message = "Longitude must be between -180 and 180")
  @Max(value = -180, message = "Longitude must be between -180 and 180")
  @Schema(description = "The longitude coordinate of the address", example = "7.4660812")
  private Float longitude;

  @NotNull(message = "The address street may not be null.")
  @NotBlank(message = "The address street may not be blank or empty.")
  @Schema(description = "The street of the address", required = true, example = "Friedensplatz")
  private String street;

  @NotNull(message = "The address house number may not be null.")
  @NotBlank(message = "The address house number may not be blank or empty.")
  @Schema(description = "The house number of the address", required = true, example = "1337")
  private String houseNumber;

  @NotNull(message = "The address city may not be null.")
  @NotBlank(message = "The address city may not be blank or empty.")
  @Schema(description = "The city of the address", required = true, example = "Dortmund")
  private String city;

  @NotNull(message = "The address ZIP code may not be null.")
  @NotBlank(message = "The address ZIP code may not be blank or empty.")
  @Schema(description = "The ZIP code of the address", required = true, example = "44135")
  private String zip;

  @NotNull(message = "The address country may not be null.")
  @NotBlank(message = "The address country may not be blank or empty.")
  @Schema(description = "The country of the address", required = true, example = "DE")
  private String country;
}
