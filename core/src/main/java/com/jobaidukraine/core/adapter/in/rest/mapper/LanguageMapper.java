package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.LanguageLevelDto;
import com.jobaidukraine.core.domain.LanguageLevel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationName = "InRestLanguageMapperImpl")
public interface LanguageMapper {
  LanguageLevel dtoToDomain(LanguageLevelDto languageLevelDto);

  LanguageLevelDto domainToDto(LanguageLevel languageLevel);
}
