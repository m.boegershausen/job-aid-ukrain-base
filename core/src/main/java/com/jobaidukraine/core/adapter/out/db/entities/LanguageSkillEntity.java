package com.jobaidukraine.core.adapter.out.db.entities;

public enum LanguageSkillEntity {
  NATIVE,
  FLUENT,
  GOOD,
  BASIC
}
