package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.JobDto;
import com.jobaidukraine.core.domain.Job;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(
    componentModel = "spring",
    implementationName = "InRestJobMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {AddressMapper.class, LanguageMapper.class})
public interface JobMapper {

  Job dtoToDomain(JobDto jobDto);

  @Mapping(source = "jobType.type", target = "jobType")
  JobDto domainToDto(Job job);
}
