package com.jobaidukraine.core.architecture;

import com.tngtech.archunit.core.domain.JavaClasses;
import java.util.ArrayList;
import java.util.List;

public class Services extends ArchitectureElement {

  private final HexagonalArchitecture parentContext;
  private final List<String> servicePackages = new ArrayList<>();

  Services(HexagonalArchitecture parentContext, String basePackage) {
    super(basePackage);
    this.parentContext = parentContext;
  }

  public HexagonalArchitecture and() {
    return parentContext;
  }

  String getBasePackage() {
    return basePackage;
  }

  void dontDependOnEachOther(JavaClasses classes) {
    List<String> allRepositories = servicePackages;
    for (String adapter1 : allRepositories) {
      for (String adapter2 : allRepositories) {
        if (!adapter1.equals(adapter2)) {
          denyDependency(adapter1, adapter2, classes);
        }
      }
    }
  }

  void doesNotDependOn(String packageName, JavaClasses classes) {
    System.out.println("asserting that " + this.basePackage + " does not depend on " + packageName);
    denyDependency(this.basePackage, packageName, classes);
  }

  void doesNotContainEmptyPackages() {
    denyEmptyPackages(servicePackages);
  }
}
