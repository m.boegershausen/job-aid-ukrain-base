package com.jobaidukraine.core.adapter.in.rest.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jobaidukraine.core.adapter.in.rest.assemblers.JobResourceAssembler;
import com.jobaidukraine.core.adapter.in.rest.dto.AddressDto;
import com.jobaidukraine.core.adapter.in.rest.dto.JobDto;
import com.jobaidukraine.core.adapter.in.rest.dto.LanguageLevelDto;
import com.jobaidukraine.core.adapter.in.rest.mapper.JobMapper;
import com.jobaidukraine.core.domain.*;
import com.jobaidukraine.core.services.ports.in.queries.JobQuery;
import com.jobaidukraine.core.services.ports.in.queries.UserQuery;
import com.jobaidukraine.core.services.ports.in.usecases.JobUseCase;
import com.jobaidukraine.core.services.ports.in.usecases.UserUseCase;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
@WebMvcTest(JobController.class)
@WithMockUser(
    username = "Picard",
    roles = {"ADMIN"})
@ActiveProfiles(profiles = "local")
class JobControllerTest {

  @Autowired MockMvc mock;

  @MockBean UserQuery userQuery;

  @MockBean UserUseCase userUseCase;

  @MockBean JobUseCase jobUseCase;

  @MockBean JobQuery jobQuery;

  @MockBean PagedResourcesAssembler<JobDto> pagedResourcesAssembler;

  @MockBean JobResourceAssembler jobResourceAssembler;

  @MockBean JobMapper jobMapper;

  static final String JOB_BASE_URL = "/v1.0/jobs";
  static final DateTimeFormatter TIMESTAMP_FORMATTER =
      DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

  @Test
  void createJobFromJson() throws Exception {
    Job job = getMockJob();
    JobDto jobDto = getMockJobDto();

    when(jobMapper.dtoToDomain(any(JobDto.class))).thenReturn(job);
    when(jobUseCase.save(any(Job.class), anyLong())).thenReturn(job);
    when(jobMapper.domainToDto(any(Job.class))).thenReturn(jobDto);
    when(jobResourceAssembler.toModel(any(JobDto.class))).thenReturn(EntityModel.of(jobDto));

    mock.perform(
            post(JOB_BASE_URL + "?companyId={id}", "1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getJobJson()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(jobDto.getId()))
        .andExpect(jsonPath("$.createdAt").value(TIMESTAMP_FORMATTER.format(jobDto.getCreatedAt())))
        .andExpect(jsonPath("$.updatedAt").value(TIMESTAMP_FORMATTER.format(jobDto.getUpdatedAt())))
        .andExpect(jsonPath("$.version").value(jobDto.getVersion()))
        .andExpect(jsonPath("$.deleted").value(jobDto.getDeleted()))
        .andExpect(jsonPath("$.title").value(jobDto.getTitle()))
        .andExpect(jsonPath("$.description").value(jobDto.getDescription()))
        .andExpect(jsonPath("$.jobType").value(jobDto.getJobType()))
        .andExpect(jsonPath("$.applicationLink").value(jobDto.getApplicationLink()))
        .andExpect(jsonPath("$.applicationMail").value(jobDto.getApplicationMail()))
        .andExpect(jsonPath("$.videoUrl").value(jobDto.getVideoUrl()))
        .andExpect(jsonPath("$.address").value(jobDto.getAddress()))
        .andExpect(jsonPath("$.qualifications").isArray())
        .andExpect(
            jsonPath("$.qualifications[0]").value(jobDto.getQualifications().iterator().next()))
        .andExpect(jsonPath("$.classification").value(jobDto.getClassification()))
        .andExpect(jsonPath("$.languageLevels").isArray());
  }

  @Test
  void readPageableListOfJobs() throws Exception {
    Job job = getMockJob();
    Job job2 = getMockJob();
    job2.setId(2L);
    Page<Job> jobPage = new PageImpl<>(List.of(job, job2));
    JobDto jobDto = getMockJobDto();
    JobDto jobDto2 = getMockJobDto();
    jobDto2.setId(2L);
    PagedModel<EntityModel<JobDto>> pagedModel =
        PagedModel.wrap(List.of(jobDto, jobDto2), new PagedModel.PageMetadata(2, 2, 2, 2));

    when(jobQuery.findAllByPageable(any(Pageable.class))).thenReturn(jobPage);
    when(jobMapper.domainToDto(any(Job.class))).thenReturn(jobDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(JobResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(get(JOB_BASE_URL))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.jobDtoes").isArray())
        .andExpect(jsonPath("$._embedded.jobDtoes").isNotEmpty())
        .andExpect(jsonPath("$._embedded.jobDtoes[0].id").value(jobDto.getId()))
        .andExpect(jsonPath("$._embedded.jobDtoes[1].id").value(jobDto2.getId()));
  }

  @Test
  void readPageableListOfJobsWithTitle() throws Exception {
    Job job = getMockJob();
    job.setId(2L);
    job.setTitle("Job Title");
    Page<Job> jobPage = new PageImpl<>(List.of(job));
    JobDto jobDto = getMockJobDto();
    jobDto.setId(2L);
    PagedModel<EntityModel<JobDto>> pagedModel =
        PagedModel.wrap(List.of(jobDto), new PagedModel.PageMetadata(2, 2, 2, 2));

    when(jobQuery.findAllByPageable(any(Pageable.class), any(String.class))).thenReturn(jobPage);
    when(jobMapper.domainToDto(any(Job.class))).thenReturn(jobDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(JobResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(get(JOB_BASE_URL + "?title={title}", "Job Title"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.jobDtoes").isArray())
        .andExpect(jsonPath("$._embedded.jobDtoes").isNotEmpty())
        .andExpect(jsonPath("$._embedded.jobDtoes[0].id").value(jobDto.getId()));
  }

  @Test
  void readPageableListOfOneJob() throws Exception {
    Job job = getMockJob();
    Page<Job> jobPage = new PageImpl<>(List.of(job));
    JobDto jobDto = getMockJobDto();
    PagedModel<EntityModel<JobDto>> pagedModel =
        PagedModel.wrap(List.of(jobDto), new PagedModel.PageMetadata(2, 2, 2, 2));

    when(jobQuery.findAllByPageable(any(Pageable.class))).thenReturn(jobPage);
    when(jobMapper.domainToDto(any(Job.class))).thenReturn(jobDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(JobResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(get(JOB_BASE_URL))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.jobDtoes").isArray())
        .andExpect(jsonPath("$._embedded.jobDtoes").isNotEmpty())
        .andExpect(jsonPath("$._embedded.jobDtoes[0].id").value(jobDto.getId()))
        .andExpect(
            jsonPath("$._embedded.jobDtoes[0].createdAt")
                .value(TIMESTAMP_FORMATTER.format(jobDto.getCreatedAt())))
        .andExpect(
            jsonPath("$._embedded.jobDtoes[0].updatedAt")
                .value(TIMESTAMP_FORMATTER.format(jobDto.getUpdatedAt())))
        .andExpect(jsonPath("$._embedded.jobDtoes[0].version").value(jobDto.getVersion()))
        .andExpect(jsonPath("$._embedded.jobDtoes[0].title").value(jobDto.getTitle()))
        .andExpect(jsonPath("$._embedded.jobDtoes[0].description").value(jobDto.getDescription()))
        .andExpect(jsonPath("$._embedded.jobDtoes[0].jobType").value(jobDto.getJobType()))
        .andExpect(
            jsonPath("$._embedded.jobDtoes[0].applicationLink").value(jobDto.getApplicationLink()))
        .andExpect(
            jsonPath("$._embedded.jobDtoes[0].applicationMail").value(jobDto.getApplicationMail()))
        .andExpect(jsonPath("$._embedded.jobDtoes[0].videoUrl").value(jobDto.getVideoUrl()))
        .andExpect(jsonPath("$._embedded.jobDtoes[0].address").value(jobDto.getAddress()))
        .andExpect(jsonPath("$._embedded.jobDtoes[0].qualifications").isArray())
        .andExpect(
            jsonPath("$._embedded.jobDtoes[0].qualifications[0]")
                .value(jobDto.getQualifications().iterator().next()))
        .andExpect(
            jsonPath("$._embedded.jobDtoes[0].classification").value(jobDto.getClassification()))
        .andExpect(jsonPath("$._embedded.jobDtoes[0].languageLevels").isArray());
  }

  @Test
  void readPageableListOfNoJob() throws Exception {
    Page<Job> jobPages = new PageImpl<>(Collections.emptyList());

    when(jobQuery.findAllByPageable(any(Pageable.class))).thenReturn(jobPages);

    mock.perform(get(JOB_BASE_URL).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.jobDtoes").doesNotExist());
  }

  @Test
  void readJob() throws Exception {
    Job job = getMockJob();
    JobDto jobDto = getMockJobDto();
    EntityModel<JobDto> entityModel = EntityModel.of(jobDto);

    when(jobQuery.findById(1L)).thenReturn(job);
    when(jobMapper.domainToDto(any(Job.class))).thenReturn(jobDto);
    when(jobResourceAssembler.toModel(any(JobDto.class))).thenReturn(entityModel);

    mock.perform(get(JOB_BASE_URL + "/{id}", 1L).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(jobDto.getId()))
        .andExpect(jsonPath("$.createdAt").value(TIMESTAMP_FORMATTER.format(jobDto.getCreatedAt())))
        .andExpect(jsonPath("$.updatedAt").value(TIMESTAMP_FORMATTER.format(jobDto.getUpdatedAt())))
        .andExpect(jsonPath("$.version").value(jobDto.getVersion()))
        .andExpect(jsonPath("$.title").value(jobDto.getTitle()))
        .andExpect(jsonPath("$.description").value(jobDto.getDescription()))
        .andExpect(jsonPath("$.jobType").value(jobDto.getJobType()))
        .andExpect(jsonPath("$.applicationLink").value(jobDto.getApplicationLink()))
        .andExpect(jsonPath("$.applicationMail").value(jobDto.getApplicationMail()))
        .andExpect(jsonPath("$.videoUrl").value(jobDto.getVideoUrl()))
        .andExpect(jsonPath("$.address").value(jobDto.getAddress()))
        .andExpect(jsonPath("$.qualifications").isArray())
        .andExpect(
            jsonPath("$.qualifications[0]").value(jobDto.getQualifications().iterator().next()))
        .andExpect(jsonPath("$.classification").value(jobDto.getClassification()))
        .andExpect(jsonPath("$.languageLevels").isArray());
  }

  @Test
  void updateJobFromJson() throws Exception {
    Job job = getMockJob();
    JobDto jobDto = getMockJobDto();
    EntityModel<JobDto> entityModel = EntityModel.of(jobDto);

    when(jobMapper.dtoToDomain(any(JobDto.class))).thenReturn(job);
    when(jobUseCase.update(any(Job.class))).thenReturn(job);
    when(jobMapper.domainToDto(any(Job.class))).thenReturn(jobDto);
    when(jobResourceAssembler.toModel(any(JobDto.class))).thenReturn(entityModel);

    mock.perform(
            patch(JOB_BASE_URL + "/{id}", 2)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getJobJson()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(jobDto.getId()))
        .andExpect(jsonPath("$.createdAt").value(TIMESTAMP_FORMATTER.format(jobDto.getCreatedAt())))
        .andExpect(jsonPath("$.updatedAt").value(TIMESTAMP_FORMATTER.format(jobDto.getUpdatedAt())))
        .andExpect(jsonPath("$.version").value(jobDto.getVersion()))
        .andExpect(jsonPath("$.title").value(jobDto.getTitle()))
        .andExpect(jsonPath("$.description").value(jobDto.getDescription()))
        .andExpect(jsonPath("$.jobType").value(jobDto.getJobType()))
        .andExpect(jsonPath("$.applicationLink").value(jobDto.getApplicationLink()))
        .andExpect(jsonPath("$.applicationMail").value(jobDto.getApplicationMail()))
        .andExpect(jsonPath("$.videoUrl").value(jobDto.getVideoUrl()))
        .andExpect(jsonPath("$.address").value(jobDto.getAddress()))
        .andExpect(jsonPath("$.qualifications").isArray())
        .andExpect(
            jsonPath("$.qualifications[0]").value(jobDto.getQualifications().iterator().next()))
        .andExpect(jsonPath("$.classification").value(jobDto.getClassification()))
        .andExpect(jsonPath("$.languageLevels").isArray());
  }

  @Test
  void deleteJob() throws Exception {
    mock.perform(
            delete(JOB_BASE_URL + "/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getJobJson()))
        .andExpect(status().is(204));

    verify(jobUseCase, times(1)).delete(1L);
  }

  Job getMockJob() {
    Long id = 1L;
    LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
    LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 12, 10);
    Integer version = 1;
    Boolean deleted = false;
    String title = "test title";
    String description = "test description";
    JobType jobType = JobType.FREELANCE;
    String applicationLink = "test-applicationlink.test";
    String applicationMail = "test@test.test";
    String videoUrl = "test-videourl.test";
    Address address = new Address();
    Set<Experience> qualifications = new HashSet<>();
    Set<LanguageLevel> languageLevels = new HashSet<>();
    return new Job(
        id,
        createdAt,
        updatedAt,
        version,
        deleted,
        title,
        description,
        jobType,
        applicationLink,
        applicationMail,
        videoUrl,
        address,
        qualifications,
        null,
        languageLevels);
  }

  JobDto getMockJobDto() {
    Long id = 1L;
    LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
    LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 12, 10);
    Integer version = 1;
    Boolean deleted = false;
    String title = "test title";
    String description = "test description";
    String jobType = "FREELANCE";
    String applicationLink = "test-applicationlink.test";
    String applicationMail = "test@test.test";
    String videoUrl = "test-videourl.test";
    AddressDto address = new AddressDto();
    Set<String> qualifications = new HashSet<>(List.of("PROFESSIONAL"));
    Set<LanguageLevelDto> languageLevels = new HashSet<>();
    return new JobDto(
        id,
        createdAt,
        updatedAt,
        version,
        deleted,
        title,
        description,
        jobType,
        applicationLink,
        applicationMail,
        videoUrl,
        address,
        qualifications,
        null,
        languageLevels);
  }

  String getJobJson() throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.writeValueAsString(getMockJobDto());
  }
}
