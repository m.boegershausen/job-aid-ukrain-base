package com.jobaidukraine.core.adapter.out.db.implementation.job;

import static org.mockito.Mockito.*;

import com.jobaidukraine.core.adapter.out.db.entities.AddressEntity;
import com.jobaidukraine.core.adapter.out.db.entities.ClassificationEntity;
import com.jobaidukraine.core.adapter.out.db.entities.ExperienceEntity;
import com.jobaidukraine.core.adapter.out.db.entities.JobEntity;
import com.jobaidukraine.core.adapter.out.db.entities.LanguageLevelEntity;
import com.jobaidukraine.core.adapter.out.db.mapper.JobMapper;
import com.jobaidukraine.core.adapter.out.db.repositories.JobRepository;
import com.jobaidukraine.core.domain.*;
import java.time.LocalDateTime;
import java.util.*;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
class JobAdapterTest {

  @Mock JobRepository jobRepository;
  @Mock JobMapper jobMapper;
  @InjectMocks JobAdapter jobAdapter;

  @Test
  void findExistingJobById() {
    JobEntity jobEntity = getJobEntityWithId(1L);
    Job job = getJobWithId(1L);

    when(jobRepository.findById(1L)).thenReturn(Optional.of(jobEntity));
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity)))).thenReturn(job);

    Optional<Job> result = jobAdapter.findById(1L);

    Assertions.assertThat(result).isPresent();
    Assertions.assertThat(result.get()).usingRecursiveComparison().isEqualTo(job);
  }

  @Test
  void findAllExistingJobsByPageable() {
    JobEntity jobEntity1 = getJobEntityWithId(1L);
    JobEntity jobEntity2 = getJobEntityWithId(2L);
    Job job1 = getJobWithId(1L);
    Job job2 = getJobWithId(2L);
    Page<JobEntity> jobEntityPage = new PageImpl<>(List.of(jobEntity1, jobEntity2));

    when(jobRepository.findAll(PageRequest.of(0, 10))).thenReturn(jobEntityPage);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity1)))).thenReturn(job1);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity2)))).thenReturn(job2);

    Page<Job> result = jobAdapter.findAllByPageable(PageRequest.of(0, 10));

    Assertions.assertThat(result.getTotalElements()).isEqualTo(2);
    Assertions.assertThat(result.getContent())
        .usingRecursiveComparison()
        .isEqualTo(List.of(job1, job2));
  }

  @Test
  void findAlExistingJobslByPageableAndTitle() {
    JobEntity jobEntity1 = getJobEntityWithId(1L);
    JobEntity jobEntity2 = getJobEntityWithId(2L);
    Job job1 = getJobWithId(1L);
    Job job2 = getJobWithId(2L);
    Page<JobEntity> jobEntityPage = new PageImpl<>(List.of(jobEntity1, jobEntity2));

    when(jobRepository.findByTitleContaining(PageRequest.of(0, 10), jobEntity1.getTitle()))
        .thenReturn(jobEntityPage);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity1)))).thenReturn(job1);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity2)))).thenReturn(job2);

    Page<Job> result = jobAdapter.findAllByPageable(PageRequest.of(0, 10), job1.getTitle());

    Assertions.assertThat(result.getTotalElements()).isEqualTo(2);
    Assertions.assertThat(result.getContent())
        .usingRecursiveComparison()
        .isEqualTo(List.of(job1, job2));
  }

  @Test
  void updateExistingJob() {
    JobEntity jobEntity = getJobEntityWithId(1L);
    Job job = getJobWithId(1L);

    when(jobRepository.findById(1L)).thenReturn(Optional.of(jobEntity));
    when(jobRepository.save(argThat(new JobEntityMatcher(jobEntity)))).thenReturn(jobEntity);
    when(jobMapper.toDomain(argThat(new JobEntityMatcher(jobEntity)))).thenReturn(job);

    Job result = jobAdapter.update(job);

    Assertions.assertThat(result).usingRecursiveComparison().isEqualTo(job);
  }

  @Test
  void deleteExistingJob() {
    JobEntity jobEntity = getJobEntityWithId(1L);

    when(jobRepository.findById(1L)).thenReturn(Optional.of(jobEntity));

    jobAdapter.delete(1L);

    verify(jobRepository).save(argThat(new JobEntityMatcher(jobEntity)));
  }

  Job getJobWithId(Long id) {
    LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
    LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 12, 10);
    Integer version = 1;
    Boolean deleted = false;
    String title = "test title";
    String description = "test description";
    JobType jobType = JobType.FREELANCE;
    String applicationLink = "test-applicationlink.test";
    String applicationMail = "test@test.test";
    String videoUrl = "test-videourl.test";
    Address address = new Address();
    Set<Experience> qualifications = new HashSet<>();
    Classification classification = null;
    Set<LanguageLevel> languageLevels = new HashSet<>();
    return new Job(
        id,
        createdAt,
        updatedAt,
        version,
        deleted,
        title,
        description,
        jobType,
        applicationLink,
        applicationMail,
        videoUrl,
        address,
        qualifications,
        classification,
        languageLevels);
  }

  JobEntity getJobEntityWithId(Long id) {
    LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
    LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 12, 10);
    Integer version = 1;
    Boolean deleted = false;
    String title = "test title";
    String description = "test description";
    String jobType = JobType.FREELANCE.name();
    String applicationLink = "test-applicationlink.test";
    String applicationMail = "test@test.test";
    String videoUrl = "test-videourl.test";
    AddressEntity address = new AddressEntity();
    Set<ExperienceEntity> qualifications = new HashSet<>(Collections.emptySet());
    ClassificationEntity classification = null;
    Set<LanguageLevelEntity> languageLevels = new HashSet<>(Collections.emptySet());
    return new JobEntity(
        id,
        createdAt,
        updatedAt,
        version,
        deleted,
        title,
        description,
        jobType,
        applicationLink,
        applicationMail,
        videoUrl,
        address,
        qualifications,
        classification,
        languageLevels);
  }

  private static class JobMatcher implements ArgumentMatcher<Job> {
    private final Job expected;

    public JobMatcher(Job expected) {
      this.expected = expected;
    }

    @Override
    public boolean matches(Job actual) {
      if (expected == null || actual == null) {
        return expected == null && actual == null;
      }

      return Objects.equals(actual.getId(), expected.getId())
          && Objects.equals(actual.getCreatedAt(), expected.getCreatedAt())
          && Objects.equals(actual.getUpdatedAt(), expected.getUpdatedAt())
          && Objects.equals(actual.getVersion(), expected.getVersion())
          && Objects.equals(actual.getDeleted(), expected.getDeleted())
          && Objects.equals(actual.getTitle(), expected.getTitle())
          && Objects.equals(actual.getDescription(), expected.getDescription())
          && Objects.equals(actual.getJobType(), expected.getJobType())
          && Objects.equals(actual.getApplicationLink(), expected.getApplicationLink())
          && Objects.equals(actual.getApplicationMail(), expected.getApplicationMail())
          && Objects.equals(actual.getVideoUrl(), expected.getVideoUrl())
          && Objects.equals(actual.getAddress(), expected.getAddress())
          && Objects.equals(actual.getQualifications(), expected.getQualifications())
          && Objects.equals(actual.getClassification(), expected.getClassification())
          && Objects.equals(actual.getLanguageLevels(), expected.getLanguageLevels());
    }
  }

  private static class JobEntityMatcher implements ArgumentMatcher<JobEntity> {
    private final JobEntity expected;

    public JobEntityMatcher(JobEntity expected) {
      this.expected = expected;
    }

    @Override
    public boolean matches(JobEntity actual) {
      if (expected == null || actual == null) {
        return expected == null && actual == null;
      }

      return Objects.equals(actual.getId(), expected.getId())
          && Objects.equals(actual.getCreatedAt(), expected.getCreatedAt())
          && Objects.equals(actual.getUpdatedAt(), expected.getUpdatedAt())
          && Objects.equals(actual.getVersion(), expected.getVersion())
          && Objects.equals(actual.getDeleted(), expected.getDeleted())
          && Objects.equals(actual.getTitle(), expected.getTitle())
          && Objects.equals(actual.getDescription(), expected.getDescription())
          && Objects.equals(actual.getJobType(), expected.getJobType())
          && Objects.equals(actual.getApplicationLink(), expected.getApplicationLink())
          && Objects.equals(actual.getApplicationMail(), expected.getApplicationMail())
          && Objects.equals(actual.getVideoUrl(), expected.getVideoUrl())
          && Objects.equals(actual.getAddress(), expected.getAddress())
          && Objects.equals(actual.getQualifications(), expected.getQualifications())
          && Objects.equals(actual.getClassification(), expected.getClassification())
          && Objects.equals(actual.getLanguageLevels(), expected.getLanguageLevels());
    }
  }
}
