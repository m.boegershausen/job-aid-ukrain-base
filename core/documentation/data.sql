-- Beispieldaten für Umkreissuche.

-- User
INSERT INTO core.user (id, country, email, experience, firstname, lastname, position)
               VALUES (1, 'Germany', 'bewerbung@adesso.de', 1, 'Hannelore', 'Bewerbung', 'Recruiter');

-- Company
INSERT INTO core.company (id, city, country, house_number, longitude, latitude, street, zip, name, user_id)
                  VALUES (1, 'Dortmund', 'Germany', '1', 51.50456203791412, 7.526979542486976, 'Adessoplatz 1', '44269', 'adesso SE', 1);

-- Job
INSERT INTO core.job (city, country, house_number, longitude, latitude, street, zip, description, job_type, title, job_id)
              VALUES ('Frankfurt', 'Germany', '1', 50.080980313465794, 8.62904308481595, 'Herriotstr.', '60528', 'Spring Boot Architekt mit Java', 'FULLTIME', 'Software Architect', 1);
INSERT INTO core.job (city, country, house_number, longitude, latitude, street, zip, description, job_type, title, job_id)
              VALUES ('Düsseldorf', 'Germany', '5', 51.2737455190341, 6.7661782541642586, 'Klaus-Bungert-Str.', '40468', 'React Developer', 'FULLTIME', 'Developer', 1);
INSERT INTO core.job (city, country, house_number, longitude, latitude, street, zip, description, job_type, title, job_id)
              VALUES ('Hamburg', 'Germany', '1', 53.54721567315044, 10.00356908491756, 'Willy-Brandt-Str.', '20457', 'Requirements Engineer für Mobile Applikationen', 'FULLTIME', 'Requirements Engineer', 1);
INSERT INTO core.job (city, country, house_number, longitude, latitude, street, zip, description, job_type, title, job_id)
              VALUES ('München', 'Germany', '1', 48.13161587123696, 11.620525811746894, 'Streitfeldstr.', '81673', 'Tester für iOS', 'FULLTIME', 'Test Engineer', 1);
