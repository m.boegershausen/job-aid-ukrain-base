-- Beispielabfragen für Umkreissuch

-- Jobs in 2km Radius um Hauptbahnhof Hamburg
select *
from job
where ST_Distance_Sphere(point(job.longitude, job.latitude), point(53.5521361305624, 10.011526882984338)) < 2000;

-- Jobs in 10km Radius um Flughafen Frankfurt
select *
from job
where ST_Distance_Sphere(point(job.longitude, job.latitude), point(50.03918612331716, 8.567469635224391)) < 10000;