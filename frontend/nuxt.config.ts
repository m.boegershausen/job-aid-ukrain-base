import { defineNuxtConfig } from 'nuxt3'

// https://v3.nuxtjs.org/docs/directory-structure/nuxt.config
export default defineNuxtConfig({
    buildModules: ['@nuxtjs/tailwindcss'],
    publicRuntimeConfig: {
        apiUrl: process.env.API_URL,
        apiUrlGeo: process.env.API_URL_GEO,
      },
})
