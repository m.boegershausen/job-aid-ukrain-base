export enum Experience {
    PROFESSIONAL = 'PROFESSIONAL',
    NEW_GRADUATE = 'NEW_GRADUATE',
    STUDENT = 'STUDENT'
}

export enum JobType {
    FREELANCE = 'FREELANCE',
    PERMANENT = 'PERMANENT',
    PART_TIME = 'PART_TIME',
    TEMPORARY = 'TEMPORARY'
}

export class Pageable {
    page: number = 0;
    size: number = 10;

    constructor(page: number = 0, size: number = 10) {
        this.page = page;
        this.size = size;
    }
}

export class JobSearchRepository {

    search = async (keywords: string, jobType: JobType, experience: Experience, lat: number, long: number, distance: number, pageable: Pageable) => {
        const config = useRuntimeConfig();
        const searchApiEndpoint: string = `${config.apiUrl}/jobsearch/?`;
        const pageableUrlSearchParam = new URLSearchParams({
            page: pageable.page.toString(),
            size: pageable.size.toString()
        });
        const searchParams = {
            q: keywords,
            type: jobType,
            exp: experience,
            lat: lat.toString(),
            long: long.toString(),
            distance: distance.toString(),
        };
        const urlSearchParams = new URLSearchParams(searchParams);

        const url = searchApiEndpoint + urlSearchParams.toString() + '&' + pageableUrlSearchParam.toString();
        const requestOptions = this.buildRequestOptions('GET');
        return await fetch(url, requestOptions);
    };

    buildRequestOptions = (requestType: string, headers: Object = this.buildHeaders()): Object => {
        return {
            method: requestType,
            headers: headers
        }
    };

    buildHeaders = (): Object => {
        return {
            'Content-Type': 'application-json'
        }
    };
}
