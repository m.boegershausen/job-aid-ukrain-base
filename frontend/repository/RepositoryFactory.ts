import { JobSearchRepository } from './JobSearchRepository';

const repositories: Object = {
    'JobSearch': JobSearchRepository,
};

export default {
    get: (repositoryName: string) => {
        return repositories[repositoryName];
    }
};
